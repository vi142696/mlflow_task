import os
import mlflow


os.environ["MLFLOW_S3_ENDPOINT_URL"] = 'http://65.21.51.166:9000'
os.environ["MLFLOW_TRACKING_URI"] = 'http://65.21.51.166:5900'
os.environ["AWS_ACCESS_KEY_ID"] = 'minio'
os.environ["AWS_SECRET_ACCESS_KEY"] = 'minio123'

MODEL_FLOAT_PATH = "models:/iris_model_sklearn/production"

model_float = mlflow.sklearn.load_model(MODEL_FLOAT_PATH)
